#ifndef ADPCM_H
#define ADPCM_H

#include "string.h"
#include "iot_debug.h"
#include "iot_types.h"

#ifdef __cplusplus
extern "C" {
#endif

#define ADPCM_BLOCK_SAMPLES 505 //for 8k wav block.sheen
#define ADPCM_BLOCK_SIZE 256 //encode block size.sheen

struct adpcm_state {
    int16_t valprev; /* Previous output value */
    char index;  /* Index into stepsize table */
};

struct adpcm_channel {
    int32_t pcmdata;                        // current PCM value
    int32_t error, weight, history [2];     // for noise shaping
    int16_t index;                           // current index into step size table
};

struct adpcm_context {
    struct adpcm_channel channels [2];
    int num_channels, lookahead, noise_shaping;
    int block_samples;//record encode samples count for block.total 505
    int tmp_count;//remain sample count.
    int16_t tmp_buf[2];//for 2 samples align. 1 ch.

};

typedef struct
{
    uint8       szRiff[4];          /* "RIFF" */
    uint32      dwFileSize;         /* file size */
    uint8       szWaveFmt[8];       /* "WAVEfmt " */
    uint32      dwFmtSize;          /* 20 before 'fact' */
    uint16        wFormatTag;         /* format type */
    uint16        nChannels;          /* number of channels (i.e. mono, stereo...) */
    uint32       nSamplesPerSec;     /* sample rate */
    uint32       nAvgBytesPerSec;    /* for buffer estimation */
    uint16        nBlockAlign;        /* block size of data */
    uint16        wBitsPerSample;     /* number of bits per sample of mono data */
    uint16        cbSize;
    uint16	  samplesPerBlock;
    uint8         fact[4]; //="fact";
    uint16        factsize_low; //=4;
    uint16        factsize_hi; //=0;
    uint16        sample_low;
    uint16        sample_hi;
} WAVE_HEADER_EX;
#define SIZE_WAVE_HEADER_EX sizeof(WAVE_HEADER_EX)

typedef struct
{
    uint8       szData[4];          /* "data" */
    uint32       dwDataSize;         /*pcm data size*/
} WAVE_DATA_HEADER;
#define SIZE_WAVE_DATA_HEADER sizeof(WAVE_DATA_HEADER)

void adpcm_encode_block_header(struct adpcm_context *pcnxt, uint8_t *outbuf, uint32_t *outbufsize, const int16_t *inbuf, uint32_t *inbufcount);
void adpcm_encode_chunks (struct adpcm_context *pcnxt, uint8_t *outbuf, uint32_t *outbufsize, int16_t *inbuf, uint32_t *inbufcount);
void adpcm_decoder(char* inbuff,char* outbuff,int len_of_in,struct adpcm_state *state );
#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif/* ADPCM_H*/
