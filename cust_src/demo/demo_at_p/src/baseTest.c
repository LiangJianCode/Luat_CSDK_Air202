#include "iot_xgd.h"

#define ddi_base_print iot_debug_print

void ddi_malloc_test(void)
{
	char *p = ddi_mem_malloc(1000);

	ddi_base_print("[baseApi-%s] p ddi_mem_malloc %x", __FUNCTION__, p);
	ddi_mem_free(p);
	ddi_base_print("[baseApi-%s] p ddi_mem_free %x", __FUNCTION__, p);
}

void ddi_time_test(void)
{
	s32 curTime = ddi_utils_stimer_get();
	u32 timeOut;
	u32 whileCount = 0;
	while(1)
	{
		// 是否超时
		timeOut = ddi_utils_stimer_query(curTime, 3000);
		whileCount++;

		//如果超时退出循环
		if (timeOut)
		{
			ddi_base_print("[baseApi-%s] %d,%d", __FUNCTION__, whileCount, timeOut);
			break;
		}

		// 没有超时延迟500毫秒
		ddi_sys_msleep(500);
		
		ddi_base_print("[baseApi-%s] %d,%d", __FUNCTION__, whileCount, timeOut);
	}
}

void ddi_base_test(void)
{
	// 测试malloc接口
	ddi_malloc_test();

	// 测试软定时和延迟接口
	ddi_time_test();
}
