#include "string.h"
#include "iot_debug.h"
#include "iot_i2c.h"

#define i2c_print 		iot_debug_print
#define DEMO_I2C_PORT  	OPENAT_I2C_3

#define SHT30_DEFAULT_ADDR         0x44
#define SHT30_DEFAULT_READ         0xE000
#define SHT30_DEFAULT_READ1		   0x240B
#define SHT30_READSTATUS           0xF32D
#define SHT30_CLEARSTATUS          0x3041
#define SHT30_SOFTRESET            0x30A2

VOID demo_writeandread(UINT16 comd, PUINT8 buf, ssize_t rdsize)
{
	ssize_t sendsize = 2;
	UINT8 cmd[sendsize];
	cmd[0] = (comd >> 8) & 0xff;
	cmd[1] = comd & 0xff;
	int len, len2;
	len2 = iot_i2c_write(DEMO_I2C_PORT, SHT30_DEFAULT_ADDR, 0, cmd, sendsize);
	len = iot_i2c_read(DEMO_I2C_PORT, SHT30_DEFAULT_ADDR, 0, buf, rdsize);
	iot_debug_print("i2c iot_i2c_write len2 %d", len2);
	iot_debug_print("i2c iot_i2c_read len %d", len);
	
}

VOID demo_gettempandhumidity(int *temp)
{
	UINT8 buf[10] = {0};
	demo_writeandread(SHT30_DEFAULT_READ1, buf, 6);
	UINT16 ST = 0, SRH = 0;
	ST = buf[0] << 8;
	ST = ST|buf[1];
	i2c_print("i2c ST = 0x%x", ST);
	*temp = -45*100 +((17500 * (int) (ST)) / (int)0xFFFF);
}

VOID demo_printtempandhumidity(VOID)
{
	int tempC;
	demo_gettempandhumidity(&tempC);
	int sw[2];
	sw[0] = tempC / 100;
	sw[1] = tempC % 100;
	i2c_print("i2c Temperature %d.%dC",sw[0],sw[1]);
}

VOID demo_i2c_open(VOID)
{
	i2c_print("i2c demo_i2c_open");
    BOOL err;
    T_AMOPENAT_I2C_PARAM i2cCfg;
    
    memset(&i2cCfg, 0, sizeof(T_AMOPENAT_I2C_PARAM));
	i2cCfg.freq = 400000;
	i2cCfg.regAddrBytes = 0;
	i2cCfg.noAck = FALSE;
	i2cCfg.noStop = FALSE;
	i2cCfg.i2cMessage = 0;

	err = iot_i2c_open(DEMO_I2C_PORT, &i2cCfg);
	iot_debug_print("i2c err %d", err);
}

VOID demo_getstatus(VOID)
{
	UINT8 buf[6] = {0};
	int i;
	demo_writeandread(SHT30_READSTATUS, buf, 3);
	for(i = 0; i < 3; i++)	
		i2c_print("i2c status buf[%d] = 0x%x", i, buf[i]);
}

VOID demo_i2c_close(VOID)
{
	iot_i2c_close(DEMO_I2C_PORT);
	i2c_print("i2c demo_i2c_close");
}

VOID demo_init(VOID)
{   
	demo_i2c_open();
	iot_os_sleep(1000);
	while(1)
	{
		demo_printtempandhumidity();
		iot_os_sleep(3000);
	}
}

VOID app_main(VOID)
{
	iot_pmd_exit_deepsleep();
	iot_debug_set_fault_mode(OPENAT_FAULT_HANG);
	iot_os_sleep(1000);
	i2c_print("[i2c] app_main");
	iot_os_sleep(1000);
    demo_init();
	demo_i2c_close();
}