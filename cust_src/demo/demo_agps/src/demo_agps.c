#include "string.h"
#include "iot_debug.h"
#include "iot_uart.h"
#include "iot_pmd.h"
#include "NmeaParser.h"
#include "RxP.h"
#include "commUtil.h"
#include "agps.h"
typedef struct {
    UINT8 Type;
    UINT8 Data;
}USER_MESSAGE;

enum
{
	USER_MSG_NETWORK,
	USER_MSG_TIMER,
};

#define sscanf IVTBL(sscanf)
#define GPS_PRINT iot_debug_print
#define DBG(X, Y...)	iot_debug_print("%s %d:"X, __FUNCTION__, __LINE__, ##Y)
static uint8_t NetReady;

static const char *GetImeiCmd = "AT+WIMEI?\r";
static const char *SetCengCmd = "AT+CENG=1,1\r";
static const char *GetCengCmd = "AT+CENG?\r";
static uint8_t LastCmdType;
extern void example_init();
extern void example_proc();

/*以下函数用于获取GPS关键数据*/
extern NMEA_RMC_T* gpsDataGetRmc(void);
extern GNSS_GSV_T* gpsDataGetGSV(void);
extern GNSS_GSA_T* gpsDataGetGSA(void);
extern NMEA_GGA_T* gpsDataGetGGA(void);

static void rxDataInd(T_AMOPENAT_UART_MESSAGE* evt)
{
  char data[1024];
  int len;
  if(evt->evtId == OPENAT_DRV_EVT_UART_RX_DATA_IND)
  {
    len = iot_uart_read(OPENAT_UART_2, data, evt->param.dataLen, 0);

    if(len > 0)
    {
      int i;
      for(i = 0; i < len; i++)
      { 
        rxp_pcrx_nmea(data[i]);
      }
    }
  }
}

static BOOL gpsIsFixed()
{

  NMEA_RMC_T* rmc = gpsDataGetRmc();
 
  
  if(rmc->cStatus == 'A')
  {
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}

static void demo_gps_task(PVOID pParameter)
{
  T_AMOPENAT_UART_PARAM uartCfg;

  UINT16 i, staUsed, staView;

  char latStr[32];
  char lonStr[32];

  NMEA_GGA_T* ggaPtr;
  GNSS_GSA_T* gsaPtr;
  GNSS_GSV_T* gsvPtr;

  memset(&uartCfg, 0, sizeof(T_AMOPENAT_UART_PARAM));
  uartCfg.baud = OPENAT_UART_BAUD_115200;
  uartCfg.dataBits = 8;
  uartCfg.stopBits = 1;
  uartCfg.parity = OPENAT_UART_NO_PARITY; 
  uartCfg.flowControl = OPENAT_UART_FLOWCONTROL_NONE; 
  uartCfg.txDoneReport = FALSE;
  uartCfg.uartMsgHande = rxDataInd;

  iot_uart_config(OPENAT_UART_2, &uartCfg);


  example_init();
    

  while(1)
  {
    staUsed = 0;
    staView = 0;
    example_proc();

    iot_os_sleep(1000);
    /*AGPS工作时，本任务不工作*/
    if (AGPSCtrl.AGPSWork)
    {
    	continue;
    }
    if (AGPSCtrl.reBootUart)
    {
    	AGPSCtrl.reBootUart = 0;
    	iot_uart_config(OPENAT_UART_2, &uartCfg);
    	continue;
    }
    /*输出GPS数据*/

    gsvPtr = gpsDataGetGSV();
    gsaPtr = gpsDataGetGSA();
    ggaPtr = gpsDataGetGGA();


    for(i = 0, staUsed = 0; i < gsaPtr->i2NumGNSS; i++)
    {
      staUsed += gsaPtr->u2ActiveGNSS[i];
    }

    for(i = 0; i < gsvPtr->i2NumGNSS; i++)
    {
      staView += gsvPtr->i2GnssStaView[i];
    }

    GPS_PRINT("[gps] satellites = (%d,%d)  signal = (%d-%d)", staUsed, staView, 
          gsvPtr->i2MinSNR, gsvPtr->i2MaxSNR);

    if(gpsIsFixed())
    {
      commUtilFormatFloat(latStr, 32, ggaPtr->dfLatitude, 6);
      commUtilFormatFloat(lonStr, 32, ggaPtr->dfLongitude, 6);
      GPS_PRINT("[gps] is fixed [mode = %cD LOC = (%s,%s)]", gsaPtr->cFixMode, 
              latStr, lonStr);
    }
    else
    {
      GPS_PRINT("[gps] is not fix");
    }
  }
  
}

static void AGPS_NetworkIndCallBack(E_OPENAT_NETWORK_STATE state)
{
    T_OPENAT_NETWORK_CONNECT networkparam;
    switch (state)
    {
    case OPENAT_NETWORK_READY:
    	NetReady = 0;
    	memset(&networkparam, 0, sizeof(T_OPENAT_NETWORK_CONNECT));
    	memcpy(networkparam.apn, "CMNET", strlen("CMNET"));
    	iot_network_connect(&networkparam);
    	break;
    case OPENAT_NETWORK_LINKED:
    	NetReady = 1;
    	break;
    default:
    	NetReady = 0;
    	break;
    }

}

static void demo_agps_task(PVOID pParameter)
{
	iot_os_sleep(1000);
	iot_vat_sendATcmd(GetImeiCmd, strlen(GetImeiCmd));
	iot_os_sleep(1000);
	iot_vat_sendATcmd(SetCengCmd, strlen(SetCengCmd));
	while(1)
	{
		iot_os_sleep(1000);
		if (NetReady)
		{
			if (GK9501_AGPSNetRun() < 0)
			{
				DBG("AGPS Fail!");
			}
			else
			{
				if (GK9501_AGPSUartRun() < 0)
				{
					DBG("AGPS Fail!");
				}
			}
			DBG("AGPS Done!");
			while(1)
			{
				iot_os_sleep(86400000);
			}
		}
		else
		{
			iot_vat_sendATcmd(GetCengCmd, strlen(GetCengCmd));
		}
	}

}

VOID vatAtIndHandle(UINT8 *pData, UINT16 length)
{
	uint32_t id, rssi, lac, ci, ta, mcc, mnc, unuse1, unuse2, unuse3,unuse4,unuse5;
	char *cengStart;
	char *cengEnd;
	char *cut;
	char *end = pData + length;
	uint8_t LAC_Pos, CI_Pos, i, j, AddFlag;
	uint8_t Temp[2];
	if(strstr(pData,"+WIMEI:")!=NULL)
	{
		cengStart = strstr(pData,"+WIMEI:");
		memset(AGPSCtrl.IMEI, 0, sizeof(AGPSCtrl.IMEI));
		memcpy(AGPSCtrl.IMEI, cengStart + 7, 15);
		DBG("IMEI %s", AGPSCtrl.IMEI);
	}
    else
    {
    	cengStart = strstr(pData, "+CENG:");
    	while(cengStart)
    	{
			if ( (uint32_t)end - (uint32_t)cengStart < 11 )
			{
				return ;
			}

    		cengEnd = strstr(cengStart, "\r");
    		if (cengEnd)
    		{
    			*cengEnd = 0;
    			if (strlen(cengStart) < 11)
    			{
    				goto CHECK_END;
    			}
    			//DBG("%s", cengStart);
    			cut = strchr(cengStart, ':');
    			if (!cut)
    			{
    				goto CHECK_END;
    			}
    			id = *(cut + 1) - '0';
    			if (!id)
    			{
    				//DBG("find first ceng");
    				memset(&AGPSCtrl.LBSInfo, 0, sizeof(AGPSCtrl.LBSInfo));
    				sscanf(cut, ":%d,\"%d,%d,%d,%d,%d,%d,%x,%d,%d,%x,%d\"",&id, &unuse1,
    						&rssi, &unuse2, &mcc, &mnc, &unuse3, &ci, &unuse4, &unuse5, &lac, &ta);
    				//DBG("%d,%d,%d,%d,%d",mcc,mnc,lac,ci,rssi);
    				if (rssi >= 99)
    				{
    					rssi = 0;
    				}
    				if (mnc == 4)
    				{
    					mnc = 0;
    				}
    				AGPSCtrl.MCC = mcc;
    				AGPSCtrl.MNC = mnc;
    				AGPSCtrl.LBSInfo.LACNum = 1;
    				AGPSCtrl.LBSInfo.LAC[0].CINum = 1;
    				AGPSCtrl.LBSInfo.LAC[0].LAC_BE[0] = lac >> 8;
    				AGPSCtrl.LBSInfo.LAC[0].LAC_BE[1] = lac & 0x00ff;
    				AGPSCtrl.LBSInfo.LAC[0].CI[0].CSQ = rssi;
    				AGPSCtrl.LBSInfo.LAC[0].CI[0].CI_BE[0] = ci >> 8;
    				AGPSCtrl.LBSInfo.LAC[0].CI[0].CI_BE[1] = ci & 0x00ff;
    			}
    			else
    			{
    				sscanf(cut, ":%d,\"%d,%d,%d,%d,%d,%d,%d,%d,%d,%x,%x\"",&id, &unuse1,
    						&rssi, &mcc, &mnc, &unuse1, &ci, &lac);
    				//DBG("%d,%d,%d,%d,%d",mcc,mnc,lac,ci,rssi);
    				if (rssi >= 99)
    				{
    					rssi = 0;
    				}
    				if (ci && lac)
    				{
    					AddFlag = 1;
    					Temp[0] = lac >> 8;
    					Temp[1] = lac & 0x00ff;
    					for(j = 0; j < AGPSCtrl.LBSInfo.LACNum; j++)
    					{
    						if (!memcmp(AGPSCtrl.LBSInfo.LAC[j].LAC_BE, Temp, 2))
    						{
    							LAC_Pos = j;
    							CI_Pos = AGPSCtrl.LBSInfo.LAC[j].CINum;
    							AGPSCtrl.LBSInfo.LAC[LAC_Pos].CI[CI_Pos].CSQ = rssi;
    		    				AGPSCtrl.LBSInfo.LAC[LAC_Pos].CI[CI_Pos].CI_BE[0] = ci >> 8;
    		    				AGPSCtrl.LBSInfo.LAC[LAC_Pos].CI[CI_Pos].CI_BE[1] = ci & 0x00ff;
    		    				AGPSCtrl.LBSInfo.LAC[j].CINum++;
    							AddFlag = 0;
    							break;
    						}
    					}
    					if (AddFlag)
    					{
    						LAC_Pos = AGPSCtrl.LBSInfo.LACNum;
    						CI_Pos = 0;
    	    				AGPSCtrl.LBSInfo.LAC[LAC_Pos].LAC_BE[0] = lac >> 8;
    	    				AGPSCtrl.LBSInfo.LAC[LAC_Pos].LAC_BE[1] = lac & 0x00ff;

    						AGPSCtrl.LBSInfo.LAC[LAC_Pos].CI[CI_Pos].CSQ = rssi;
		    				AGPSCtrl.LBSInfo.LAC[LAC_Pos].CI[CI_Pos].CI_BE[0] = ci >> 8;
		    				AGPSCtrl.LBSInfo.LAC[LAC_Pos].CI[CI_Pos].CI_BE[1] = ci & 0x00ff;
    						AGPSCtrl.LBSInfo.LACNum++;
    						AGPSCtrl.LBSInfo.LAC[LAC_Pos].CINum = 1;
    					}
    				}
    			}

CHECK_END:
				cengStart = cengEnd + 1;
				if ( (uint32_t)end - (uint32_t)cengStart < 7 )
				{
					return ;
				}
				cengStart = strstr(cengStart, "+CENG:");
    		}
    	}
    }
}

VOID app_main(VOID)
{
	iot_vat_init(vatAtIndHandle);
	iot_network_set_cb(AGPS_NetworkIndCallBack);
    /*1, 打开GPS供电和32K时钟*/
    iot_pmd_poweron_ldo(OPENAT_LDO_POWER_CAM, 7);
    IVTBL(sys32k_clk_out)(1);
    /*2, 新建TASK解析GPS串口数据*/
    iot_os_create_task(demo_gps_task,
                        NULL,
                        20480,
                        5,
                        OPENAT_OS_CREATE_DEFAULT,
                        "demo_gps");
    iot_os_create_task(demo_agps_task,
            NULL,
            20480,
            5,
            OPENAT_OS_CREATE_DEFAULT,
            "demo_agps");
    iot_pmd_exit_deepsleep();//仅为了调试方便，不允许进入休眠模式
    iot_debug_set_fault_mode(OPENAT_FAULT_HANG);
}
