#include "string.h"
#include "iot_os.h"
#include "iot_debug.h"

#define MMI_print  iot_debug_print

VOID demo_getsystime(VOID)
{	
	T_AMOPENAT_SYSTEM_DATETIME curTime;
	iot_os_get_system_datetime(&curTime);
    MMI_print("[wywy] now %u:%u:%u %u:%u:%u", 
        curTime.nYear, curTime.nMonth, curTime.nDay,
        curTime.nHour, curTime.nMin, curTime.nSec);
}

VOID demo_MMIprint(VOID)
{
	demo_getsystime();
	MMI_print("[wywy]ONE Minute.");
}

VOID demo_Start(VOID)
{
	
	PMINUTE_TICKFUNC demo_MMIfunc = demo_MMIprint;
	iot_os_get_minute_tick(demo_MMIfunc);
}

VOID app_main(VOID)
{
	iot_os_sleep(1000);
	MMI_print("[wywy] app_main");
	demo_Start();
}
