#include "string.h"
#include "iot_debug.h"
#include "iot_uart.h"

#define CMD_AT  "AT\r\n"
#define CMD_CREG  "AT+CREG?\r\n"
#define CMD_AMGSMLOC "AT+AMGSMLOC\r\n"
#define CMD_CSQ "AT+CSQ\r\n"
#define CMD_TTS_OPEN "AT+QTTS=1\r\n"
#define CMD_TTS_PLAY "AT+QTTS=2,\"300031003200\"\r\n"
#define CMD_AMRT_PLAY "AT+AMRT=1,1,0\r\n"
#define CMD_AMRT_STOP "AT+AMRT=0\r\n"

HANDLE g_netWorkTimer;

#define DEMO_RECORD_CH OPENAT_AUD_CHANNEL_LOUDSPEAKER
VOID vatAtIndHandle(UINT8 *pData, UINT16 length)
{
	iot_debug_print("[tts] AT RECV: %s", pData);

    //注册网络成功
    if (strstr(pData, "+CREG: 0,1") || strstr(pData, "+CREG: 2,1"))
    {
        // 基站查询
        iot_os_stop_timer(g_netWorkTimer);
        iot_debug_print("[tts] send %s", CMD_CSQ);
        iot_vat_sendATcmd(CMD_CSQ, strlen(CMD_CSQ)); 
        iot_debug_print("[tts] send %s", CMD_AMGSMLOC);
        iot_vat_sendATcmd(CMD_AMGSMLOC, strlen(CMD_AMGSMLOC)); 
    }
    // 基站查询失败
    else if (strstr(pData, "+AMGSMLOC: 1")) 
    {
        // 基站查询
        iot_debug_print("[tts] send %s", CMD_AMGSMLOC);
        iot_vat_sendATcmd(CMD_AMGSMLOC, strlen(CMD_AMGSMLOC));
    }
        
}

VOID demo_audio_set_channel(VOID)
{
    // 设置通道

    iot_debug_print("[tts] iot_audio_get_current_channel %d", iot_audio_get_current_channel());
    switch(DEMO_RECORD_CH)
    {
         case OPENAT_AUD_CHANNEL_LOUDSPEAKER:
         default:   
            iot_audio_set_channel(OPENAT_AUD_CHANNEL_LOUDSPEAKER);
            iot_audio_set_speaker_gain(OPENAT_AUD_SPK_GAIN_18dB);
            iot_audio_set_channel_with_same_mic(OPENAT_AUD_CHANNEL_HANDSET, OPENAT_AUD_CHANNEL_LOUDSPEAKER);
            break;   
    }

    iot_debug_print("[tts] AUDREC channel %d", DEMO_RECORD_CH);
}

VOID netWorkTimerHandle(T_AMOPENAT_TIMER_PARAMETER *pParameter)
{
    iot_debug_print("[tts] send %s", CMD_CREG);
    iot_vat_sendATcmd(CMD_CREG, strlen(CMD_CREG));
    iot_os_start_timer(g_netWorkTimer, 1000);
}

VOID app_main(VOID)
{
    // 初始化内部通道
    iot_vat_init(vatAtIndHandle);
    // 初始化音频通道
    demo_audio_set_channel();

    // 定是查询注册网络状态
    g_netWorkTimer = iot_os_create_timer(netWorkTimerHandle, NULL);
    iot_os_start_timer(g_netWorkTimer, 1000);

    //发送AT
    iot_os_sleep(2000);
    iot_debug_print("[tts] send %s", CMD_AT);
    iot_vat_sendATcmd(CMD_AT, strlen(CMD_AT));

    //发送tts初始化
    iot_os_sleep(2000);
    iot_debug_print("[tts] send %s", CMD_TTS_OPEN);
    iot_vat_sendATcmd(CMD_TTS_OPEN, strlen(CMD_TTS_OPEN));

    //发送tts播放
    iot_os_sleep(2000);
    iot_debug_print("[tts] send %s", CMD_TTS_PLAY);
    iot_vat_sendATcmd(CMD_TTS_PLAY, strlen(CMD_TTS_PLAY));

    //发送来电音播放
    iot_os_sleep(2000);
    iot_debug_print("[tts] send %s", CMD_AMRT_PLAY);
    iot_vat_sendATcmd(CMD_AMRT_PLAY, strlen(CMD_AMRT_PLAY));

    // 发送来电音停止播放
    iot_os_sleep(2000);
    iot_debug_print("[tts] send %s", CMD_AMRT_STOP);
    iot_vat_sendATcmd(CMD_AMRT_STOP, strlen(CMD_AMRT_STOP));
}
