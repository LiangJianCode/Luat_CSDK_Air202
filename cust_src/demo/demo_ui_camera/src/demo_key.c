#include "string.h"
#include "stdio.h"
#include "iot_keypad.h"
#include "iot_debug.h"
#include "iot_pmd.h"
#include "demo_zbar.h"
#include "disp.h"
#include "demo_camera.h"

extern APP_STATUS g_app_status;

char keypadValue[4][4] = 
{
  {'1','4','7','*'},
  {'2','5','8','0'},
  {'3','6','9','#'},
  {'A','B','C','D'}
};

char keypadValueTest[4][4] = 
{
  {'1','2','3','4'},
  {'5','6','7','8'},
  {'9','A','B','C'},
  {'D','*','0','#'}
};

VOID keypad_callback(T_AMOPENAT_KEYPAD_MESSAGE *pKeypadMessage)
{ 
  int r = pKeypadMessage->data.matrix.r;
  int c = pKeypadMessage->data.matrix.c;

  if (pKeypadMessage->bPressed)
  {
    iot_debug_print("[zbar] keypad_callback key (%d,%d,%c), g_app_status %d", 
        r, c, keypadValue[r][c], g_app_status);
  
    switch(g_app_status)
    {
      case APP_STATUS_IDLE:
      {
        switch(keypadValue[r][c])
        {
          // ��������
          case '1':
          {
            g_app_status = APP_STATUS_KEY;
            disp_keypad_test_begin();
            break;
          }
          // ɨ�����
          case '2':
          {
            g_app_status = APP_STATUS_ZBAR;
            if (camera_preview_open())
            {
              disp_zbar_test_begin();
            }
            break;
          }
          case '3':
          {
            g_app_status = APP_STATUS_PREVIEW;
            camera_preview_open();
            break;
          }
          default:
            break;
        }

        break;
      }
      case APP_STATUS_KEY:
      {
        static int i = 0;
        char *value = (char *)keypadValueTest;
        iot_debug_print("[zbar] keypadValue %c,%c", keypadValue[r][c], value[i]);
        if (keypadValue[r][c] == value[i])
        {
          i++;
          char count[3];
          
          memset(count, 0, 3);
          sprintf(count, "%d", i);
         
          disp_keypad_test_process(count);
          
          if (i == 16)
          {
            i = 0;
            disp_keypad_test_end(TRUE);
            g_app_status = APP_STATUS_SELECT;
          }
        }
        else
        {
          i=0;
          g_app_status = APP_STATUS_SELECT;
          disp_keypad_test_end(FALSE);
        }

        break;
      }
      case APP_STATUS_ZBAR:
      {
        if (camera_preview_is_open())
          break;
        
        switch(keypadValue[r][c])
        {
          case 'A':
          {
            g_app_status = APP_STATUS_IDLE;
            disp_idle();
            break;
          }
          case 'B':
          {
            g_app_status = APP_STATUS_ZBAR;
            if (camera_preview_open())
            {
              disp_zbar_test_begin();
            }
            break;
          }
          default:
            break;
        }
        break;
      }
      case APP_STATUS_PREVIEW:
      {
        iot_debug_print("[zbar] APP_STATUS_PREVIEW end");
        camera_preview_close();
        iot_os_sleep(200);
        g_app_status = APP_STATUS_IDLE;
        disp_idle();
        break;
      }  
      case APP_STATUS_SELECT:
      {
        iot_debug_print("[zbar] APP_STATUS_SELECT");
        switch(keypadValue[r][c])
        {
          case 'A':
          {
            g_app_status = APP_STATUS_IDLE;
            disp_idle();
            break;
          }
          case 'B':
          {
            g_app_status = APP_STATUS_KEY;
            disp_keypad_test_begin();
            break;
          }
          default:
            break;
        }
        break;
      }
      default:
        break;
    }
  
    
  }
}


void keypad_init(void)
{
  T_AMOPENAT_KEYPAD_CONFIG pConfig;
  BOOL err;
  
  iot_pmd_poweron_ldo(OPENAT_LDO_POWER_KEYPDA, 5);

  memset(&pConfig, 0, sizeof(pConfig));
  
  pConfig.type = OPENAT_KEYPAD_TYPE_MATRIX;
  pConfig.pKeypadMessageCallback = keypad_callback;
  pConfig.config.matrix.keyInMask = 0xff;
  pConfig.config.matrix.keyOutMask = 0xff;

  err = iot_keypad_init(&pConfig);

  iot_debug_print("[zbar] keypad_init err %d", err);

}
