#ifndef __DEMO_CAMERA_H__
#define __DEMO_CAMERA_H__

void camera_preview_close(void);
BOOL  camera_preview_is_open(void);
BOOL camera_preview_open(void);
void camera_open(void);

#endif
